#!/usr/bin/env python

"""This is the docstring of module tsoin.__main__."""

import sys

if __name__ == "__main__":
    print("Running python module tsoin.__main__ " + " ".join(sys.argv[1:]))
