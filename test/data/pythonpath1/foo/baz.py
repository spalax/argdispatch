#!/usr/bin/env python

import sys

if __name__ == "__main__":
    print("Running python module foo.baz " + " ".join(sys.argv[1:]))
