"""Docstring of module withmain.__main__."""

import sys

if __name__ == "__main__":
    print("Running python module withmain.__main__ " + " ".join(sys.argv[1:]))
