"""Docstring of module withmain.baz."""

import sys

if __name__ == "__main__":
    print("Running python module withmain.baz " + " ".join(sys.argv[1:]))
